module.exports = {
    env: "dev",//当前环境
    mysql: { //mysql数据库
        host: '127.0.0.1',
        user: 'root', //你安装的数据库用户名
        password: 'root', //你安装的mysql数据库密码
        database: 'blog', //数据库
        port: 3306, //端口
        },
    };